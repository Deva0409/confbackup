#!/bin/bash

find . -maxdepth 1 -mindepth 1 ! -regex "./\(config_files.txt\|.git\|copy.sh\)" | xargs rm –r
cat config_files.txt | xargs -I {} cp -- parents -R {} .
